<?php

namespace App\BITM\SEIP117343;

use \App\BITM\SEIP117343\Message;
use App\BITM\SEIP117343\Utility;

class Birthday {

    public $id = "";
    public $title = "";
    public $birth = "";

    /*  public function __construct($data=array()) {
      if (is_array($data) && array_key_exists('author', $data)) {
      $this->title= $data['title'];
      $this->author= $data['birth'];
      }

      } */

    public function index() {
        $_books = array();
        //echo "hi";
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("birthday") or die("Cannot select database.");
        $query = "SELECT * FROM `birth`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_books[] = $row;
        }
        return $_books;
    }

    public function store() {
        //echo $title, $birth;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("birthday") or die("Cannot select database.");
        $query = "INSERT INTO `birthday`.`birth` ( `title`,`birth`) VALUES ( '" . $this->title . "','" . $this->birth . "')";
        //echo $query;
        if (mysql_query($query)) {
            Message::set('Birthday data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        header('location:index.php');
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("birthday") or die("Cannot select database.");
        $query = "SELECT * FROM `birth` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("birthday") or die("Cannot select database.");
        $query = "SELECT * FROM `birth` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function update() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("birthday") or die("Cannot select database.");
        $query = "UPDATE `birth` SET `title` = '" . $this->title . "', `birth` = '" . $this->birth . "' WHERE `birth`.`id` = " . $this->id;
//echo $query;
//die();
        if (mysql_query($query)) {
            Message::set('Birthday data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("birthday") or die("Cannot select database.");
        $query = "DELETE FROM `birthday`.`birth` WHERE `birth`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Birthday data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->birth = $data['birth'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
