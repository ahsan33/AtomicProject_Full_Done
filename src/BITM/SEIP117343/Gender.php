<?php

namespace App\BITM\SEIP117343;

use \App\BITM\SEIP117343\Message;
use App\BITM\SEIP117343\Utility;

class Gender {

    public $id = "";
    public $title = "";
    public $gender = "";

    public function index() {
        $_books = array();
        //echo "hi";
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("gender") or die("Cannot select database.");
        $query = "SELECT * FROM `gender_select`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_books[] = $row;
        }
        return $_books;
    }

    public function store() {
        //echo $title, $gender;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        //echo $conn;
        $lnk = mysql_select_db("gender") or die("Cannot select database.");
        $query = "INSERT INTO `gender`.`gender_select` ( `title`,`gender`) VALUES ( '" . $_REQUEST['title'] . "','" . $_REQUEST['gender'] . "')";
        //echo $query;
        if (mysql_query($query)) {
            Message::set('Gender data added successfuly');
        } else {
            Message::set('Error,please try again');
        }
        header('location:index.php');
    }

    public function show($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("gender") or die("Cannot select database.");
        $query = "SELECT * FROM `gender_select` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
    }

    public function edit($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("gender") or die("Cannot select database.");
        $query = "SELECT * FROM `gender_select` WHERE id=" . $id;
        $b = mysql_query($query);
        $result = mysql_fetch_object($b);
        return $result;
        // $result= mysql_fetch_object($b);
        // Utility::dd($result);
        //Utility::dd();
    }

    public function update() {
        //echo $title, $author;
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("gender") or die("Cannot select database.");
        $query = "UPDATE `gender_select` SET `title` = '" . $this->title . "', `gender` = '" . $this->gender . "' WHERE `gender_select`.`id` = " . $this->id;
//echo $query;
//die();
        if (mysql_query($query)) {
            Message::set('Gender data updated successfuly');
        } else {
            Message::set('Error,please try again');
        }
        Utility::redirect("index.php");
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            return;
        }
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
//echo $conn;
        $lnk = mysql_select_db("gender") or die("Cannot select database.");
        $query = "DELETE FROM `gender`.`gender_select` WHERE `gender_select`.`id`=" . $id;
        if ($b = mysql_query($query)) {
            Message::set('Gender data deleted successfuly');
        } else {
            Message::set('Error,please try again');
        }
        //$result=  mysql_fetch_object($b);
        //return $result;
        Utility::redirect("index.php");
    }

    public function prepare($data = array()) {
        //Utility::dd($data);
        if (is_array($data) && array_key_exists('title', $data)) {

            $this->title = $data['title'];
            $this->gender = $data['gender'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}
