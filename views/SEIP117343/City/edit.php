<?php


include_once ("../../../"."vendor/autoload.php");
use \App\BITM\SEIP117343\City;
use App\BITM\SEIP117343\Utility;

$book= new City();
$books=$book->edit($_GET['id']);
//var_dump($books);
//die();
//Utility::dd($books);
?>


<html>
    <head>
        <title>Edit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <form action="update.php" method="post" class="form-inline">
                <div class="col-md-6">
                    <div><br>
                        <input type="hidden" class="form-control" 
                               name="id"
                               id="id" 
                               value="<?php echo $books->id;?>">
                        <label for="name">Book name:  </label>
                        <input type="text" class="form-control" 
                               name="title"
                               id="name" 
                               value="<?php echo $books->title;?>">
                    </div><br>
                    
                    <div>
                        <label for="id">City name: </label>
                         <select class="form-control" name="city">
                             <option  name="city" value="Sylhet"<?php if(preg_match('/Sylhet/', $books->city)){ echo "selected";} ?>>Sylhet</option>
                            <option  name="city" value="Dhaka"<?php if(preg_match('/Dhaka/', $books->city)){ echo "selected";} ?>>Dhaka</option>
                            <option name="city" value="Khulna"<?php if(preg_match('/Khulna/', $books->city)){ echo "selected";} ?>>Khulna</option>
                            <option name="city" value="Sunamganj"<?php if(preg_match('/Sunamganj/', $books->city)){ echo "selected";} ?>>Sunamganj</option>
                            <option name="city" value="Rangpur"<?php if(preg_match('/Rangpur/', $books->city)){ echo "selected";} ?>>Rangpur</option>
                            
                       
    
                               
                               
                             </select>  <?php  $books->city;?>

                        <button type="submit" class="btn btn-primary">Update</button>
                    </div></div>



            </form>
            
        </div><div><center><button type="button" class="btn btn-info"><a href="index.php">Go to list</a></button></center></div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
    </body>
</html>

