<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
      <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">   
        
    </head>
    <body>
     <div class="container">
            <form action="store.php" method="post" class="form-inline" enctype="multipart/form-data">
                <div class="col-md-6">
                     <div class="panel-body">
                        <div class="col-md-6">
                            <label for="name">Enter name:  </label>
                            <input type="text" class="form-control" 
                               name="name"
                               id="name" 
                               placeholder="Name" 
                               required="required">
                        </div><br>
                     </div>   
                    <div class="form-group">
                        <div class="input-group">
                            <label for="image">Upload image:  </label>
                            <input type="file" name="image" class="form-control" id="image">

                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                          
                </div>
            </form>
         </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
    </body>
</html>
